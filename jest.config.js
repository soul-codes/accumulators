import { resolve } from "path";

import { readFile } from "fs/promises";

const { pathsToModuleNameMapper } = (
  await import("ts-jest/utils/index.js")
).default;
const tsconfigPath = resolve("src/test", "tsconfig.json");

export default {
  globals: {
    "ts-jest": {
      tsconfig: tsconfigPath,
    },
  },
  transform: {
    "^.+\\.tsx?$": "ts-jest",
    "^.+\\.js$": "babel-jest",
  },
  transformIgnorePatterns: ["node_modules/(?!lodash-es)"],
  testMatch: ["<rootDir>/src/test/**/*.test.ts"],
  verbose: true,
  moduleNameMapper: pathsToModuleNameMapper(
    JSON.parse(await readFile(tsconfigPath)).compilerOptions.paths
  ),
};
