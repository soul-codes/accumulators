import { Accumulator } from "./@types.js";
import { accumulator } from "./accumulator.js";

export const diff = <T, U>(
  diffFn: (current: T, previous: T) => U,
  initial: T
): Accumulator<T, U> =>
  accumulator(
    (current, _, previous) => [diffFn(current, previous), current],
    null as any, // never used
    initial
  );
