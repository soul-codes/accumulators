export interface Accumulator<T, U> {
  (current: T): U;
}

export interface Comparer<T> {
  (a: T, b: T): boolean;
}
