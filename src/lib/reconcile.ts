import { Comparer } from "./@types.js";
import { accumulator } from "./accumulator.js";

/**
 * Creates an accumulator that returns the oldest given value as long as it is
 * equal to the input value according to the comparer.
 * @param comparer
 */
export const reconcile = <T>(comparer: Comparer<T>) =>
  accumulator(
    (current: T, previous: T, hasPrevious: boolean) => [
      hasPrevious && comparer(current, previous) ? previous : current,
      true,
    ],
    null as any, // never used
    false
  );
