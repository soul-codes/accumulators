import { Accumulator } from "./@types.js";
import { accumulator } from "./accumulator.js";

/**
 * Creates an accumulator that returns the value previously given.
 * @param initial
 */
export const lag = <A>(initial: A): Accumulator<A, A> =>
  accumulator((current, _, previous) => [previous, current], initial, initial);
