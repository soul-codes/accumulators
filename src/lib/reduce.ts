import { Accumulator } from "./@types.js";
import { accumulator } from "./accumulator.js";

/**
 * Creates an accumulator that merges the input with a previously merged value,
 * starting from the initial seed.
 * @param reducer
 * @param initialReduced
 */
export const reduce = <A, B>(
  reducer: (current: A, previousReduced: B) => B,
  initialReduced: B
): Accumulator<A, B> =>
  accumulator(
    (current, previous) => [reducer(current, previous), null],
    initialReduced,
    null
  );
