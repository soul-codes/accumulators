import { Accumulator } from "./@types.js";

export const accumulator = <A, B, S>(
  accumulateFn: (
    current: A,
    previousValue: B,
    previousState: S
  ) => readonly [B, S],
  initialAccumulated: B,
  initialState: S
): Accumulator<A, B> => {
  let accumulated = initialAccumulated;
  let state = initialState;
  return (current) =>
    ([accumulated, state] = accumulateFn(current, accumulated, state))[0];
};
