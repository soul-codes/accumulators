import { reduce } from "@lib";

test("reduce", () => {
  const acc = reduce((next: number, sum) => next + sum, 0);
  expect(acc(1)).toEqual(1);
  expect(acc(1)).toEqual(2);
  expect(acc(4)).toEqual(6);
});
