import { reconcile } from "@lib";
import { isEqual } from "lodash";

test("reconcile", () => {
  const acc = reconcile(isEqual);
  const initial = { foo: 1 };
  expect(acc(initial)).toEqual(initial);
  expect(acc({ foo: 1 })).toEqual(initial);

  const next = { foo: 2 };
  expect(acc(next)).toEqual(next);
  expect(acc({ foo: 2 })).toEqual(next);
});
