import { diff } from "@lib";

test("diff", () => {
  const acc = diff((current, previous) => current - previous, 0);
  expect(acc(1)).toEqual(1);
  expect(acc(1)).toEqual(0);
  expect(acc(4)).toEqual(3);
  expect(acc(0)).toEqual(-4);
});
