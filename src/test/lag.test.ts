import { lag } from "@lib";

test("lag", () => {
  const acc = lag(0);
  expect(acc(1)).toEqual(0);
  expect(acc(8)).toEqual(1);
  expect(acc(-2)).toEqual(8);
  expect(acc(0)).toEqual(-2);
});

test("lag can be composed", () => {
  const acc1 = lag(0);
  const acc2 = lag(0);
  expect(acc2(acc1(1))).toEqual(0);
  expect(acc2(acc1(2))).toEqual(0);
  expect(acc2(acc1(3))).toEqual(1);
  expect(acc2(acc1(7))).toEqual(2);
  expect(acc2(acc1(-10))).toEqual(3);
});
